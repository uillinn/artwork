# Uillinn artwork

This project groups all artworks belonging to Uillinn:

  * The [logo](./logo/uillinn-logo.svg)

## License

All the files in this project are released under <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Attribution-ShareAlike 4.0 International License" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>